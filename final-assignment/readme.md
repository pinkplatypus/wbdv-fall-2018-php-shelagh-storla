## Shelagh Storla
05NOV2018
CPNT 262 - PHP
Final Assignment

***

Notes:
-The "Add Content" link is under the profile view.

-Like/Unlike is Bookmark/Unbookmark.

-Replaced Lorem Picsum with Lorem Flickr to fix broken image issue.

-If you open the console on the profile page, the profile image will correct to circluar (unsure why it loads oblong).

-I added a feature that lists all the bookmarks on the user page just to show that this function is working. I was unable to get to the AJAX on the bookmark feature so when you click the bookmark button it reloads the page. And when you reload the page, it loads 10 new articles at random. So, I wanted to show that it was actually updating the database in case one of the active users bookmarked articles did not load on the main feed.

EC2 Deployment - [35.183.95.148](http://35.183.95.148 "Deployment")
