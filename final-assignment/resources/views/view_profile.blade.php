@extends('layouts.header')

@section('content')

<section class="mt-5 mb-5">

    <div class="container flex justify-content-center">

        <div class="w-75 flex justify-content-center hr">
        
            <div class="mr-4">
                <div class="flex align-top">
                    <h2 class="profile-title dark mb-3">{{ $user->name }}</h2>
                    @if (Auth::check() && Auth::id() === $user->id)
                        <a href="/update_profile" class="green-btn ml-3">Update Profile</a>
                    @endif
                </div>
                <p class="profile">{{ $user->profile->description }}</p>
                <p>Medium member since {{ $user->created_at->format('F Y') }}</p>
            </div>
        <div>
        </div>
            <img src="{{ $user->profile->image }}" alt="{{ $user->name }}" class="author-image">
        </div>
  
    </div>

</section>

<div class="container flex space-between">
    <section class="mr-5">
        <div>
            <h2 class="sub-head dark mb-3">Bookmarks</h2>
        </div>
        @foreach ($bookmarks as $bookmark)
            <div class="flex space-between mb-4">
                <div class="mr-3">
                    <div>
                        <h3 class="article-title dark mb-1">
                            <a href="/read/{{ $bookmark->id }}" class="dark">
                                {{ $bookmark->title }}
                            </a>
                        </h3>
                    </div>
                    <div>
                        By 
                        <a href="/user/{{ $bookmark->user->id }}" class="dark">
                            <span class="dark">{{ $bookmark->user->name }}</span>
                        </a>
                    </div>
                </div>
                <div>
                    @if (Auth::check() && Auth::id() === $user->id)
                        <a href="/read/bookmark/{{ $bookmark->id }}">
                            <i class="far fa-trash-alt"></i>
                        </a>
                    @endif
                </div>
            </div>
        @endforeach
    </section>

    <section>
        <div class="flex space-between align-top">
            <h2 class="sub-head dark mb-3">Articles</h2>
            @if (Auth::check() && Auth::id() === $user->id)
                <a href="/new_article" class="green-btn ml-3 mb-3">Create Article</a>
            @endif
        </div>
        @foreach ($articles as $article)
            @include('partials.articles')
        @endforeach
    </section>
</div>


@endsection
