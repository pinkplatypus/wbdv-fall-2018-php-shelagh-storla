@extends('layouts.header')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Update Article - <span class="dark">{{ $article->title }}</span>
                </div>

                <div class="card-body">
                    <form method="POST" action="/update_article">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="id" type="hidden" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ $article->id }}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old ('title', $article->title) }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Featured Image (URL)') }}</label>

                            <div class="col-md-6">
                                <input id="image" type="text" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" value="{{ old ('image', $article->image) }}" required>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="snippet" class="col-md-4 col-form-label text-md-right">{{ __('Tag Line') }}</label>

                            <div class="col-md-6">
                                <input id="snippet" type="text" class="form-control{{ $errors->has('snippet') ? ' is-invalid' : '' }}" name="snippet" value="{{ old ('snippet', $article->snippet) }}" required>

                                @if ($errors->has('snippet'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('snippet') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('Content') }}</label>

                            <div class="col-md-6">
                                <textarea id="content" type="text" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" rows="20" required>{{ old ('content', $article->content) }}</textarea>

                                @if ($errors->has('content'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="bookmarks" type="hidden" class="form-control{{ $errors->has('bookmarks') ? ' is-invalid' : '' }}" name="bookmarks" value="{{ $article->bookmarks()->count() }}" >
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="green-btn">
                                    {{ __('Submit Article') }}
                                </button>
                                <a class="green ml-3" href="/user/<?php echo request()->user()->id ?>">
                                    {{ __('Cancel') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
