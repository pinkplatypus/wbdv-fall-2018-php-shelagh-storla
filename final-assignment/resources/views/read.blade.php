@extends('layouts.header')

@section('content')
<section class="mt-5">
    <div class="container flex flex-v align-center">
        <div class="flex justify-content-center mb-5">
            <div class="align-self-end article-content">
                <div class="mb-3">
                    <h2 class="featured-title dark">{{ $article->title }}</h2>
                    <p>{{ $article->snippet }}</p>
                </div>
                <div class="flex mb-5">
                    <img src="{{ $article->user->profile->image }}" alt="Author" class="author-image-sm mr-2">
                    <div>
                        <p class="dark mb-0">
                            <a href="/user/{{ $article->user->id }}" class="dark">
                                {{ $article->user->name }}
                            </a>
                        </p>
                        <p>{{ $article->created_at->format('M j') }}  · {{ $article->time }}  min read</p>
                        <div class="flex">
                            <p class="mr-5">Bookmarked {{ $article->bookmarks()->count() }} time(s).</p>
                            @if (Auth::check() && (Auth::id() !== $article->user->id))      
                                @if (request()->user()->bookmarkedArticles->contains($article->id))
                                    <p>
                                        <a href="/read/bookmark/{{ $article->id }}">            
                                            <i class="fas fa-bookmark fa-lg green"></i>
                                        </a>
                                    </p>
                                @else
                                    <p>
                                        <a href="/read/bookmark/{{ $article->id }}">
                                            <i class="far fa-bookmark fa-lg dark"></i>
                                        </a>
                                    </p>
                                @endif
                            @endif
                        </div>   
                    </div>
                </div>
            </div>
            <div>
                <img src="{{ $article->image }}" alt="" class="featured-image">
            </div>
        </div>
        <div class="full-article w-75">
            <p>
            {{ $article->content }} 
            </p>
            <p>
            {{ $article->content }} 
            </p>
            <p>
            {{ $article->content }} 
            </p>
        </div>
    </div>
</section>
@endsection