@extends('layouts.header')

@section('content')
<main class="flex">
    <section>

        @include('partials.featured')

        <div class="latest">
            <h2 class="sub-head hr dark">Latest</h2>
            @foreach ($articles as $article)
                @include('partials.articles')
            @endforeach

    </section>

    <section class="ml-56">

        <div>
            <h2 class="topic-title mb-2">Technology</h2>
            <p>The download.</p>
            <p><a href="#" class="dark-btn">Follow</a></p>
            <p class="mt-14">Follow to get great stories about Technology in your inbox and on your homepage</p>
        </div>

        <div>
            <h2 class="sub-head hr dark mt-5">Related Topics</h2>
            <div class="flex flex-v">
                <a href="" class="uppercase mb-2">Software Engineering</a>
                <a href="" class="uppercase mb-2">Programming</a>
                <a href="" class="uppercase mb-2">Artificial Intelligence</a>
                <a href="" class="uppercase mb-2">Blockchain</a>
                <a href="" class="uppercase mb-2">Cryptocurrency</a>
            </div>
        </div>

        <div>
            <h2 class="sub-head hr mt-5 dark">Popular in Technology</h2>

            @foreach ($popular as $item)
                @include('partials.popular')
            @endforeach
        </div>

    </section>
</main>
@endsection
