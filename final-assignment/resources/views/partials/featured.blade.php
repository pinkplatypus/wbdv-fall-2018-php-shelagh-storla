<div class="featured">
    <h2 class="sub-head dark">Featured</h2>
    <div>
        <a href="/read/{{ $featured->id }}" class="dark">
            <img src="{{ $featured->image }}" alt="Featured Image" class="featured-image mt-14">
            <h3 class="featured-title mt-14 mb-1 dark">
                {{ $featured->title }}
            </h3>
        </a>
        <p>{{ $featured->snippet }}..</p>
    </div>
    <div class="flex mb-5">
        <a href="/user/{{ $featured->user->id }}">
            <img src="{{ $featured->user->profile->image }}" alt="Author" class="author-image-sm mr-2">
        </a>
        <div>
            <a href="/user/{{ $featured->user->id }}" class="dark">
                <p class="dark mb-0">{{ $featured->user->name }}</p>
            </a>
            <p>{{ $featured->created_at->format('M j') }} · {{ $featured->time }} min read</p>
        </div>
    </div>
</div>
