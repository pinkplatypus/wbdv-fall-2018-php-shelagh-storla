<div class="article flex space-between mb-3">
    <div class="article-content">
        <h3 class="article-title dark mb-1">
            <a href="/read/{{ $article->id }}" class="dark">
                {{ $article->title }}
            </a>
        </h3>
        <p>{{ $article->snippet }}..</p>
        <div class="flex space-between">
            <div class="flex">
                <p class="dark mr-2">
                    <a href="/user/{{ $article->user->id }}" class="dark">
                        {{ $article->user->name }}
                    </a>
                </p>
                <p>- {{ $article->time }} min read</p>
            </div>
        </div>
        @if (Auth::check() && Auth::id() === $article->user->id)
            <div>
                <a href="{{ route('update_article', ['id' => $article->id]) }}" class="green">
                    Update Article
                </a>
            </div>
        @endif
    </div>
    <div class="align-self-end">
            @if (Auth::check() && (Auth::id() !== $article->user->id))      
                @if (request()->user()->bookmarkedArticles->contains($article->id))
                    <a href="/read/bookmark/{{ $article->id }}">            
                        <i class="fas fa-bookmark fa-lg green"></i>
                    </a>
                @else
                    <a href="/read/bookmark/{{ $article->id }}">
                        <i class="far fa-bookmark fa-lg dark"></i>
                    </a>
                @endif
            @endif    
        </div>
    <div>
        <a href="/read/{{ $article->id }}">
            <img src="{{ $article->image }}" alt="Article Image" class="article-image ml-16">
        </a>
    </div>
</div>
