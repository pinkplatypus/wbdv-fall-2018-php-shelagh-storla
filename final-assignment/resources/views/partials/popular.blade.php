<div class="flex space-between mb-3">
    <div>
        <h3 class="pop-title dark mb-1">
            <a href="/read/{{ $item->id }}" class="dark">
                {{ $item->title }}
            </a>
        </h3>

        <p>{{ $item->time }} min read</p>
    </div>
    <div>
        <a href="/read/{{ $featured->id }}" class="dark">
            <img src="{{ $item->image }}" alt="Popular" class="article-image-sm">
        </a>
    </div>
</div>
