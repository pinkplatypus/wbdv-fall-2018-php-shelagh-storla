<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Final Assignment</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/app.css">
</head>
<body>

    <header>
        <div class="container">
            <nav class="flex space-between align-center">
                <div class="flex align-bottom">
                    <h1 class="vr"><a href="/" class="dark">Medium</a></h1><h2 class="topic-title-main">Technology</h2>
                </div>

                @if (Auth::check())
                    <div class="flex align-center ">
                        <a href="/user/{{ request()->user()->id }}" class="mr-4">
                        <span class="green">
                            {{ request()->user()->name }}
                        </span>
                            <img src="{{ request()->user()->profile->image }}" alt="{{ request()->user()->name }}" class="author-image-sm mr-4">
                        </a>
                        @include('partials.logout')
                    </div>
                @else
                    <div>
                        <a href="/login" class="ml-16">Sign in</a>
                        <a href="/register" class="green-btn ml-16">Get started</a>
                    </div>
                @endif
            </nav>
        </div>
    </header>

    <div class="container mb-5">
            @yield('content')
    </div>
    
</body>
</html>
    