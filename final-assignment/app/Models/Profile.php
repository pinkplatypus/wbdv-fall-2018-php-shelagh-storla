<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $primaryKey = "user_id";

    public $dates = ['joined'];

    public $table = 'user_profiles';

    protected $fillable = [
        'user_id', 'image', 'name', 'description', 'location',
    ];
}
