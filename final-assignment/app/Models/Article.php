<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $dates = ['published'];

    public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function bookmarks() 
    {
        return $this->belongsToMany('App\User');
    }
}
