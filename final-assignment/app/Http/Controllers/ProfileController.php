<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\User;


class ProfileController extends Controller
{
    public function index($id) 
    {
        $user = $this->getUser($id);
        
        $articles = $this->getUserArticles($user);

        $bookmarks = $this->getBookmarks($user->id);

        $viewData = [
            'articles' => $articles,
            'user' => $user,
            'bookmarks' => $bookmarks,
        ];

        return view ('view_profile', $viewData);
    }

    public function getUser($id = 1) 
    {
        $user = User::findOrFail($id);

        return $user;
    }

    public function getUserArticles($user) 
    {
        $articles = Article::where('user_id', $user->id)->get();

        return $articles;
    }

    public function getBookmarks($id) {
        $user = User::find($id);

        $bookmarks = $user->bookmarkedArticles;

        return $bookmarks;
    }

    public function viewProfileUpdateForm() 
    {
        if(\Auth::check()){
            $user = request()->user();
    
            $profile = $user->profile;
    
            $viewData = [
                'user' => $user,
                'profile' => $profile,
            ];
    
            return view ('update_profile', $viewData);  
        } else {
            return redirect('login');
        }
    }

    public function update() 
    {
        $formData = request()->all();
        $user = request()->user();

        $updatedProfile = $user->profile;

        $updatedProfile->name = $formData['name'];
        $updatedProfile->image = $formData['image'];
        if($formData['description']){
            $updatedProfile->description = $formData['description'];
        } else {
            $updatedProfile->description = '';
        }
        if($formData['location']){
            $updatedProfile->location = $formData['location'];
        } else {
            $updatedProfile->location = '';
        }
        
        $updatedProfile->save();
        
        return redirect ('/user/'.$user->id);
    }

}
