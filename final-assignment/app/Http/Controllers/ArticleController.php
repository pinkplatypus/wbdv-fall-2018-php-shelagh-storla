<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Article;
use App\Models\Profile;
use Faker\Factory;

class ArticleController extends Controller
{
    public function index() //Obtain Main View
    {
        $featuredArticle = $this->getFeatured();
        $articles = $this->getArticles();
        $popularArticles = $this->getPopular();
    
        $viewData = [
            'featured' => $featuredArticle,

            'articles' => $articles,

            'popular' => $popularArticles
        ];

        return view ('welcome', $viewData);
    }

    public function getFeatured() 
    {

        $featuredArticle = Article::inRandomOrder()->take(1)->get();
        $featuredArticle = $featuredArticle->last();

        return $featuredArticle;
    }

    public function getArticles()
    {
        $articles = Article::inRandomOrder()->take(10)->get();
        $articles = $articles->sortByDesc('date');

        return $articles;
    }

    public function getPopular() 
    {
        $popularArticles = Article::inRandomOrder()->take(4)->get();

        return $popularArticles;
    }

    public function readArticle($id = 1) 
    {
        $article = Article::findOrFail($id);

        $viewData = [
            'article' => $article,
        ];

        return view ('read', $viewData);
    }

    public function getNewArticleForm() 
    {
        if(\Auth::check()) {
            return view ('new_article');
        } else {
            return redirect('login');
        }
    }

    public function createNewArticle() 
    {
        request()->validate([
            'title' => 'required|string|max:255',
            'image' => 'required|string|max:255',
            'snippet' => 'required|string|max:255',
            'content' => 'required',
        ]);

        $data = request()->all();
        $user = request()->user();
        
        $article = new Article;
        $article->title = $data['title'];
        $article->image = $data['image'];
        $article->snippet = $data['snippet'];
        $article->content = $data['content'];
        $article->time = rand(2, 15);
        $article->user_id = $user->id;
        $article->save();

        return redirect('/read/'.$article->id);
    }

    public function viewUpdateArticleForm($id) 
    {
        if(\Auth::check()) {
            $article = Article::where('id', $id)->first();

            $viewData = [
                'article' => $article,
            ];

            return view ('update_article', $viewData);
        } else {
            return redirect('login');
        }
    }

    public function update()
    {
        $data = request()->all();
        $user = request()->user();

        $updatedArticle = Article::where('id', $data['id'])->first();

        $updatedArticle->title = $data['title'];
        $updatedArticle->image = $data['image'];
        $updatedArticle->snippet = $data['snippet'];
        $updatedArticle->content = $data['content'];

        $updatedArticle->save();

        return redirect('/read/'.$data['id']);      

    }

    public function toggleBookmark($articleId) 
    {
        $user = request()->user();

        if ($user->bookmarkedArticles->contains($articleId)){
            $user->bookmarkedArticles()->detach($articleId);
        } else {
            $user->bookmarkedArticles()->attach($articleId);
        }

        return redirect()->back();
    }

}
