<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles() 
    {
        return $this->hasMany('App\Models\Article');
    }

    public function profile() 
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function bookmarkedArticles() 
    {
        return $this->belongsToMany('App\Models\Article');
    }


}
