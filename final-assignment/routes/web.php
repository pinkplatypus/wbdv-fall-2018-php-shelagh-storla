<?php

// @author Shelagh Storla
// @date 05NOV2018
// CPNT 262 - PHP
// Final Assignment

use Faker\Factory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@index');
Route::get('/read/{id}', 'ArticleController@readArticle');
Route::get('/new_article', 'ArticleController@getNewArticleForm');
Route::post('/new_article', 'ArticleController@createNewArticle');
Route::get('/update_article/{id}', 
            [
                'uses' => 'ArticleController@viewUpdateArticleForm',
                'as' => 'update_article'
            ]);
Route::post('/update_article', 'ArticleController@update');
Route::get('/read/bookmark/{id}', 'ArticleController@toggleBookmark');

Route::get('/user/{id}', 'ProfileController@index');
Route::get('/update_profile', 'ProfileController@viewProfileUpdateForm');
Route::post('/update_profile', 'ProfileController@update');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
