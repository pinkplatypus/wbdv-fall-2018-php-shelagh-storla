<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Profile;
use App\Models\Article;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 25; $i++) {
            $user = new User();
            $user->email = $faker->email;
            $user->name =  $faker->name;
            $user->password = bcrypt('12341234');
            $user->save();
    
            $userProfile = new Profile();
            $userProfile->user_id = $user->id;
            $userProfile->image = 'https://loremflickr.com/200/200/?random='.rand(1, 100);
            $userProfile->name = $user->name;
            $userProfile->description = $faker->text($maxNbChars = 150);
            $userProfile->location = $faker->city.', '.$faker->stateAbbr;
            $userProfile->save();

            $count = rand(0, 10);
            for ($j=0; $j < $count; $j++) {
                $article = new Article();
                $article->user_id = $user->id;                
                $article->title = $faker->sentence;
                $article->image = 'https://loremflickr.com/800/500/?random='.rand(1, 100);
                $article->snippet = $faker->sentences($nb = 2);
                $article->snippet = join(' ', $article->snippet);
                $article->content = $faker->paragraphs($nb = 4);
                $article->content = join(' ', $article->content);
                $article->time = rand(1, 15);
                $article->save();
            }
        }

        foreach(User::all() as $user) {
            foreach(Article::all() as $article) {
                $rand = rand(1, 100);
                if ($rand < 20) {
                $user->bookmarkedArticles()->attach($article);
                }
            }
        }
    }
}
