#CPNT262/PHP
##Journal

Shelagh Storla

***

### Day 1 - 23 OCT 2018

While it appears straight-forward and has several things in common with JavaScript, there are definitely some things I need to learn.
I did run into problems with newline characters - \n - and how they factor into the code during a Loop Exercise. I THINK I understand the purpose of them, but definitely want to add it to my list of things I want to know more about and how they will come up in different languages. 

Syntax differences to remember:
    *  . - concatenate strings
    *  $ - always prefixes a variable

***

### Day 2 - 24 OCT 2018

This morning I had a brief struggle with rsort(). This built-in function reverse-sorts an array but the function itself returns a boolean. So, if you assign the sorted array to a new variable, the variable will be a boolean and not the new array.

Array functions do not work on boolean variables - duh.

-

rsort($array);

**NOT**

$newArray = rsort($array);

***

For the afternoon, the FIZZBUZZ exercise also proved challenging. I have come up with a solution, but it's dirty and full of "if" statements I am sure I don't need. After I had come up with my own solution, I looked at a classmate's solution and it was definitely far cleaner, more elegant, but appears to only useable in the test condition. 
I will re-factor my code to be cleaner and take advantage of the ideas I got from my classmates.

***

### Day 3 - 25 OCT 2018

Completed my slight re-factor on the FIZZBUZZ exercise this morning. Thing to remember when creating Arrays with For Loops, the inital array should be OUTSIDE the for loop, or every time you are just re-starting. Cameron helped me with this as I didn't understand why "join" wasn't working.
Sqola accepted it and I believe it would also work outside of the test condition as well!

***

### Day 4 - 26 OCT 2018

Due to a Doctor's Appointment, I missed about 2 1/2 hours of instruction this morning. Thankfully, Cameron has comprehensive screencasts. I was able to catch-up fairly quickly by watching the videos. No struggles with the content today!

Subscribed to the PHP subreddit this weekend - what an active community!

***

### Day 5 - 29 OCT 2018

Between lessons today, spent most of my day on the Mid-Term Assignment. The layout/styling took the most time for me. It is hard to decide when to style a full element or just use utility classes - I need to find what's make the most logical sense to me.

The PHP was fairly easy up to this point, in my opinion. Hopefully that is the case and I am not just blissfully ignorant to all the things I am doing wrong!

***

### Day 6 - 30 OCT 2018

Started Databases today! Connected our Twitter Clones to a database and pulled in the data to our views. Seems pretty straight-forward to me. The data was coming in sequentially as we added it, so I looked into the Laravel docs on how to sort a collection so that the newest tweet appeared first. I struggled briefly with figuring out where to add the method for it to work properly, but was able to nail it down.

***

### Day 7 - 31 OCT 2018
     .-.
    (o o) boo!
    | O \
     \   \
      `~~~'

***

### Day 8 - 01 NOV 2018
Today, I struggled with implementing the Laravel User. It turns out I missed one instance of referring to our own user which was breaking everything. I am not entirely sure I would have figured that out in a timely fashion without Cameron's eyes. The error message didn't really help me figure out the source of the problem, so I would probably be staring for hours!

At least I know what to look for in the future.

***

### Day 9 - 02 NOV 2018
Ahhhhhhhhhhhhhhhhhhhhhhhhh VALIDATION! I am having a ridiculous time trying to get my twitter clone to ignore the current handle when updating the profile. Gah.

***

### Day 10 - 05 NOV 2018

Worked on deployment today! Was able to get up and deployed without too many issues. Missed a line of code in the initial instructions which caused me problems down the line. Cameron sorted me out here.

***

### Day 11 - 06 NOV 2018

Final Day with Cameron! Always so quick in a fast-track program.
Today I am working on my final project. I am trying to figure out the like/unlike feature but I haven't found any clear resources yet for the functionality.

-

I found a tutorial online that made sense in order to get the like/unlike function working but it involved making another controller and a model and several other steps. Once Cameron went over his way it was, once again, far more elegant than the solution I found. I have decided this a great example of Jr Dev vs Sr Dev and I can't wait until elegant solutions are far more second nature to me.

I have reached a point where it is no longer coming to me quite as easily as before and I think that is a great thing. This is when I really start to learn! I need to build a couple more apps with PHP and Laravel to push myself more.

“I don't count my sit-ups; I only start counting when it starts hurting because they’re the only ones that count.”
― Muhammad Ali
