<?php

// @author Shelagh Storla
// @date 29OCT2018
// CPNT 262 - PHP
// Mid-Term Assignment

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;

class Article {}

class ArticleController extends Controller
{
    public function index() //Obtain View
    {
        $featuredArticle = $this->getFeatured();
        $articles = $this->getArticles();
        $popularArticles = $this->getPopular();
    
        $viewData = [
            'featured' => $featuredArticle,

            'articles' => $articles,

            'popular' => $popularArticles
        ];

        return view ('welcome', $viewData);
    }

    public function getFeatured() 
    {
        $faker = Factory::create();

        $featuredArticle = new Article();
        
        $featuredArticle->title = 'The Problem With Fixing WhatsApp? Human Nature Might Get in the Way';
        $featuredArticle->snippet = $faker->paragraph($nbSentences = 3);
        $featuredArticle->image = 'https://miro.medium.com/max/1360/1*uJGSdazEgaYYpNEya12S9Q.jpeg';
        $featuredArticle->authorImg = 'https://miro.medium.com/fit/c/80/80/0*1RdzRMFIcpA4_jg4.jpeg';
        $featuredArticle->authorName = 'Farhad Manjoo';
        $featuredArticle->date = 'Oct 26';
        $featuredArticle->time = 6;

        return $featuredArticle;
    }

    public function getArticles()
    {
        $faker = Factory::create();

        $article1 = new Article();
        $article1->title = 'IBM will acquire Red Hat for $34 Billion';
        $article1->snippet = $faker->paragraph($nbSentences = 3);
        $article1->image = 'https://miro.medium.com/max/320/1*Weg1Labhg8noAR2vu7nRlg.jpeg';
        $article1->authorName = 'Michael K. Spencer';
        $article1->time = 2;

        $article2 = new Article();
        $article2->title = 'An introduction to SOLID, Tim Berners-Lee’s new, re-decentralized Web';
        $article2->snippet = $faker->paragraph($nbSentences = 3);
        $article2->image = 'https://miro.medium.com/max/320/1*P4F0K6HR2L0VfQZmMvYt0g.png';
        $article2->authorName = 'Arnav Bansal';
        $article2->time = 4;

        $article3 = new Article();        
        $article3->title = '10 amazing cases of IoT applications taken from the real life';
        $article3->snippet = $faker->paragraph($nbSentences = 3);
        $article3->image = 'https://miro.medium.com/max/320/1*ye0F5gqrrSV3vUV1fDf22A.png';
        $article3->authorName = 'Paul Stokes';
        $article3->time = 6;

        $article4 = new Article();
        $article4->title = 'Iterating on a 3D Model';
        $article4->snippet = $faker->paragraph($nbSentences = 3);
        $article4->image = 'https://miro.medium.com/max/320/1*a1U6HM4UJlAAeYcD3VgUMQ.jpeg';
        $article4->authorName = 'Karen McClellan';
        $article4->time = 3;

        return [$article1, $article2, $article3, $article4];
    }

    public function getPopular() 
    {
        $popular1 = new Article();
        $popular1->title = 'I’m Leaving Google — Here’s the Real Deal Behind Google Cloud';
        $popular1->image = 'https://miro.medium.com/max/110/1*trNhoe9XVKt2IrSy4JbIkQ.jpeg';
        $popular1->time = 7;

        $popular2 = new Article();
        $popular2->title = 'The Big Disruption';
        $popular2->image = 'https://miro.medium.com/max/110/1*vP1im_nzYoMHlOUgkC27ew.jpeg';
        $popular2->time = 352;

        $popular3 = new Article();
        $popular3->title = 'Why the NSA Called Me After Midnight and Requested My Source Code';
        $popular3->image = 'https://miro.medium.com/max/110/1*m064Xjkvw09VgT6EwvVH7Q.jpeg';
        $popular3->time = 7;

        $popular4 = new Article();
        $popular4->title = 'I Know the Salaries of Thousands of Tech Employees';
        $popular4->image = 'https://miro.medium.com/max/110/1*AtnEn4kwZ7TLo9-7l8rGyA.jpeg';
        $popular4->time = 6;

        return [$popular1, $popular2, $popular3, $popular4];
    }
}
