<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mid-Term Assignment</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/app.css">
</head>
<body>

    <header>
        <div class="container">
            <nav class="flex space-between align-center">
                <div class="flex align-bottom">
                    <h1 class="vr">Medium</h1><h2 class="topic-title-main">Technology</h2>
                </div>
                <div>
                    <a href="#">Become a member</a>
                    <a href="#" class="ml-16">Sign in</a>
                    <a href="#" class="green-btn ml-16">Get started</a>
                </div>
            </nav>
        </div>
    </header>

    <div class="container">
        <main class="flex">
            @yield('content')
        </main>
    </div>
    
</body>
</html>
    