@extends('layout')

@section('content')
    <section>

        @include('featured')

        <div class="latest">
            <h2 class="sub-head hr dark">Latest</h2>
            <?php foreach ($articles as $article): ?>
                @include('articles')
            <?php endforeach; ?>
        </div>

    </section>

    <section class="ml-56">

        <div>
            <h2 class="topic-title mb-2">Technology</h2>
            <p>The download.</p>
            <button type="button" class="btn btn-outline-dark">Follow</button>
            <p class="mt-14">Follow to get great stories about Technology in your inbox and on your homepage</p>
        </div>

        <div>
            <h2 class="sub-head hr dark mt-5">Related Topics</h2>
            <div class="flex flex-v">
                <a href="" class="uppercase mb-2">Software Engineering</a>
                <a href="" class="uppercase mb-2">Programming</a>
                <a href="" class="uppercase mb-2">Artificial Intelligence</a>
                <a href="" class="uppercase mb-2">Blockchain</a>
                <a href="" class="uppercase mb-2">Cryptocurrency</a>
            </div>
        </div>

        <div>
            <h2 class="sub-head hr mt-5 dark">Popular in Technology</h2>

            <?php foreach ($popular as $item): ?>
                @include('popular')
            <?php endforeach; ?>
        </div>

    </section>
@endsection
