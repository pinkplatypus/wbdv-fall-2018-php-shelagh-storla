<div class="flex space-between mb-3">
    <div>
        <h3 class="pop-title dark mb-1"><?php echo $item->title ?></h3>
        <p><?php echo $item->time ?> min read</p>
    </div>
    <div>
        <img src="<?php echo $item->image ?>" alt="Popular" class="article-image-sm">
    </div>
</div>
