<div class="featured">
    <h2 class="sub-head dark">Featured</h2>
    <div>
        <img src="<?php echo $featured->image ?>" alt="Featured Image" class="featured-image mt-14">
        <h3 class="featured-title mt-14 mb-1 dark"><?php echo $featured->title ?></h3>
        <p><?php echo $featured->snippet ?>..</p>
    </div>
    <div class="flex mb-5">
        <img src="<?php echo $featured->authorImg ?>" alt="Author" class="author-image-sm mr-2">
        <div>
            <p class="dark mb-0"><?php echo $featured->authorName ?></p>
            <p><?php echo $featured->date ?> · <?php echo $featured->time ?> min read</p>
        </div>
    </div>
</div>
