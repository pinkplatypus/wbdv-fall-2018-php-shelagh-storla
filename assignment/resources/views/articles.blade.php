<div class="article flex mb-3">
    <div class="article">
        <h3 class="article-title dark mb-1"><?php echo $article->title ?></h3>
        <p><?php echo $article->snippet ?>..</p>
        <div class="flex space-between">
            <div class="flex">
            <p class="dark mr-2"><?php echo $article->authorName ?></p>
            <p><?php echo $article->time ?> min read</p>
            </div>
            <div>
            <i class="far fa-bookmark fa-lg dark"></i>
            </div>
        </div>
    </div>
    <div>
        <img src="<?php echo $article->image ?>" alt="Article Image" class="article-image ml-16">
    </div>
</div>
