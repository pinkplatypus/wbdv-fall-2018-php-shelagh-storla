<?php

// @author Shelagh Storla
// @date 29OCT2018
// CPNT 262 - PHP
// Mid-Term Assignment

use Faker\Factory;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

class Article {};

Route::get('/', 'ArticleController@index');
