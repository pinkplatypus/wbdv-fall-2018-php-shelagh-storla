<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\User;

class TweetsController extends Controller
{
    public function splash() {
        $randomUser = User::inRandomOrder()->take(1)->get();
        $randomUser = $randomUser->last();
        $splashPage = '<h1>Welcome to Twitter</h1><p>Get a <a href="/'.$randomUser->id.'">Random User</a>';
        return $splashPage;
    }

    public function index($id) {

        $primaryUser = $this->getPrimaryUser($id);

        $recommendedUsers = $this->getRecommendedUsers($primaryUser);

        $tweets = $this->getTweets($primaryUser);
        
        $viewData = [
            'user' => $primaryUser,

            'recommendedUsers' => $recommendedUsers,

            'tweets' => $tweets
        ];

        return view('welcome', $viewData);
    }


    public function getPrimaryUser($id = 1) {
        $primaryUser = User::findOrFail($id);
        $primaryUser->moments = '0';

        return $primaryUser;
    }

    public function getRecommendedUsers($primaryUser) {
        $recommendedUsers = User::where('id', '!=', $primaryUser->id)
                                ->inRandomOrder()
                                ->take(5)
                                ->get();

        return $recommendedUsers;
    }

    public function getTweets($primaryUser) {

        $tweets = Tweet::where('user_id', $primaryUser->id)->get();
        $tweets = $tweets->sortByDesc('date');

        return $tweets;

    }
}
