<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Models\Profile;

class ProfileController extends Controller
{

    public function index() 
    {
        $user = request()->user();

        $profile = $user->profile;

        $viewData = [
            'user' => $user,
            'profile' => $profile,
        ];

        return view('profile', $viewData);
    }

    public function update() 
    {
        $formData = request()->all();
        $user = request()->user();

       request()->validate([
            'handle' => 'required|unique:user_profiles|max:25',
        ]);

        $replaceChar = array("@", " ");
        $handle = $formData['handle'];
        $handle = str_replace($replaceChar, "", $handle);

        $updatedProfile = $user->profile;

        $updatedProfile->handle = '@'.$handle;

        if ($formData['heroImage']) {
            $updatedProfile->heroImage = $formData['heroImage'];
        }
        if ($formData['image']) {
            $updatedProfile->image = $formData['image'];
        }
        if($formData['website']) {
        $updatedProfile->website = $formData['website'];
        } else {
            $updatedProfile->website = '';
        }
        if($formData['description']){
            $updatedProfile->description = $formData['description'];
        } else {
            $updatedProfile->description = '';
        }
        if ($formData['location']) {
            $updatedProfile->location = $formData['location'];
        } else {
            $updatedProfile->location = '';
        }
               
        $updatedProfile->save();

        return redirect('/'.$user->id);
    }


}
