<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $primaryKey = 'user_id';
    
    public $dates = ['joined'];

    public $table = 'user_profiles';

    protected $fillable = [
        'user_id', 'image', 'heroImage', 'name', 'handle', 'website', 'description', 'location',
    ];

}
