<div class="flex flex-v">
    <div class="flex pl-15 pr-15">
        <div class="pt-10">
            <img src="<?php echo $tweet->user->profile->image ?>" alt="" class="circular img-sm">
        </div>
        <div class="mlr-10">
            <div class="flex pt-10">
                <div class="fw-bold pr-s">
                    <?php echo $tweet->user->name ?>
                </div>
                <div class="pr-s c-1">
                    <?php echo $tweet->user->profile->handle ?>
                </div>
                <div class="c-1">· 
                    <?php echo $tweet->created_at->format('M j') ?>
                </div>
            </div>
            <div>
                <?php echo $tweet->content ?>
            </div>
            <div class="flex">
                <div class="pr-15"><i class="far fa-comment"></i> 12</div>
                <div class="pr-15"><i class="fas fa-retweet"></i> 5</div>
                <div class="pr-15"><i class="far fa-heart"></i> <?php echo $tweet->likes->count() ?></div>
            </div>
        </div>
    </div>
</div>
