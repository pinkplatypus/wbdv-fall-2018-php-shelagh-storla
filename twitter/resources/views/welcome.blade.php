@extends('layout')

@section('content')
        <div class="hero bg-2" style="background-image: url('<?php echo $user->profile->heroImage ?>');">
        </div>
        <div class="stats bg-white fw-bold c-1 pt-10 pb-10 mb-10 border-bottom-shadow">
            <div class="flex justify-center">
                <div class="stat pr-25">
                    <div class="label fz-s">
                        Tweets
                    </div>
                    <div class="value fz-m">
                        <?php echo $user->tweets->count() ?>
                    </div>
                </div>
                <div class="stat pr-25">
                    <div class="label fz-s">
                        Following
                    </div>
                    <div class="value fz-m">
                        <?php echo $user->follows->count() ?>
                    </div>
                </div>
                <div class="stat pr-25">
                    <div class="label fz-s">
                        Followers
                    </div>
                    <div class="value fz-m">
                        <?php echo $user->followedBy->count() ?>
                    </div>
                </div>
                <div class="stat pr-25">
                    <div class="label fz-s">
                        Likes
                    </div>
                    <div class="value fz-m">
                        <?php echo $user->likedTweets->count() ?>
                    </div>
                </div>
                <div class="stat pr-25">
                    <div class="label fz-s">
                        Moments
                    </div>
                    <div class="value fz-m">
                        <?php echo $user->moments ?>
                    </div>
                </div>
                <?php if (Auth::check() && Auth::id() === $user->id): ?>
                    <a href="/profile" class="btn btn-outline-info">Update Profile</a>
                <?php endif; ?>
            </div>
        </div>
    </header>

    <main class="flex">
        <div class="user-details flex-1 pl-50 pr-25 mt-50">
            <div class="relative">
                <img src="<?php echo $user->profile->image ?>" alt="<?php echo $user->name ?>" class="circular profile-img" />
            </div>
            <div class="fz-l fw-bold">
                <?php echo $user->name ?>
            </div>
            <div class="c-1">
                <?php echo $user->profile->handle ?>
            </div>
            <div class="ptb-20">
                <?php echo $user->profile->description ?>
            </div>
            <?php if ($user->profile->location != ''): ?>
            <div class="c-1 pb-10">
                    <i class="fas fa-map-marker-alt"></i> 
                    <?php echo $user->profile->location ?>
            </div>
            <?php endif; ?>
            <?php if ($user->profile->website != ''): ?>
                <div class="c-1 pb-10">
                    <i class="fas fa-link"></i> 
                    <a href=""><?php echo $user->profile->website ?></a>
                </div>
            <?php endif; ?>
            <div class="c-1 pb-10">
            <i class="far fa-calendar-alt"></i> Joined <?php echo $user->created_at->format('F Y') ?>
            </div>
        </div>
        <div class="tweets bg-white half-width" style="height: auto;">
            <div class="title-bar flex fw-bold fz-m pt-10 pb-10 border-bottom">
                <div class="title pl-15 pr-25">Tweets</div>
                <div class="title pr-25"><a href="">Tweets & replies</a></div>
                <div class="title pr-25"><a href="">Media</a></div>
            </div>
            <?php foreach($tweets as $tweet): ?>
                @include('tweet')
            <?php endforeach; ?>
        
        </div>
        <div class="suggestions flex-1">
            <div class="bg-white mlr-10">
                <div class="title pt-10 pb-10 pl-15 fw-bold fz-m">You may also like</div>
                <?php foreach ($recommendedUsers as $recommendedUser): ?>
                    @include('altUser')
                <?php endforeach; ?>
            </div>
        </div>
    </main>
@endsection





</body>
</html>



