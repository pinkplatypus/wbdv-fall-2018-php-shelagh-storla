@extends('layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Update Your Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="/profile">
                        @csrf

                        <div class="form-group row">
                            <label for="handle" class="col-md-4 col-form-label text-md-right">{{ __('Handle') }}</label>

                            <div class="col-md-6">
                                <input id="handle" type="text" class="form-control{{ $errors->has('handle') ? ' is-invalid' : '' }}" name="handle" value="<?php echo old('handle', $user->profile->handle) ?>" required autofocus>

                                @if ($errors->has('handle'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('handle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="heroImage" class="col-md-4 col-form-label text-md-right">{{ __('Header Image') }}</label>

                            <div class="col-md-6">
                                <input id="heroImage" type="text" class="form-control{{ $errors->has('heroImage') ? ' is-invalid' : '' }}" name="heroImage" value="<?php echo $user->profile->heroImage ?>">

                                @if ($errors->has('heroImage'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('heroImage') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

                            <div class="col-md-6">
                                <input id="image" type="text" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" value="<?php echo $user->profile->image ?>">

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="website" class="col-md-4 col-form-label text-md-right">{{ __('Website') }}</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" value="<?php echo $user->profile->website ?>">

                                @if ($errors->has('website'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="<?php echo $user->profile->description ?>">

                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="<?php echo $user->profile->location ?>">

                                @if ($errors->has('location'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update Your Profile') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
