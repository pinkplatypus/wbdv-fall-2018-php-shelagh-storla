<div class="flex pl-15 pt-10 mb-10">
    
        <div class="pr-s">
        <a href="/<?php echo $recommendedUser->id ?>">
            <img src="<?php echo $recommendedUser->profile->image ?>" alt="" class="circular img-sm" />
        </div>
        <div class="flex flex-v">
            <div class="fw-bold"><?php echo $recommendedUser->name ?></div>
            <div class="c-1"><?php echo $recommendedUser->profile->handle ?></div>
        </a>
        </div>
    
</div>
