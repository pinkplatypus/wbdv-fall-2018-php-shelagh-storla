<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Twitter Clone</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/app.css">
</head>
<body>

    <header class="flex flex-v">
        <div class="top-top bg-white pt-10 pb-10">
            <div class="container">
                <div class="flex">
                    <?php if (Auth::check()): ?>
                        <div class="pr-25">
                        <p>Hello, <a href="/<?php echo request()->user()->id ?>"><?php echo request()->user()->name ?></a></p>
                        </div>
                        @include('logout')
                    <?php else: ?>
                        <a href="/login" class="btn btn-outline-info">Login</a>
                    <?php endif; ?>
                    <div class="pr-25">
                    <a href="#"><i class="fab fa-twitter"></i> Home</a>
                    </div>
                    <div>
                    <a href="#"><i class="fas fa-bolt"></i> Moments</a>
                    </div>
                </div>
            </div>
        </div>
        @yield('content')
